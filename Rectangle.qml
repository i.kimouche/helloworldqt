import QtQuick 2.0

Rectangle {
    id: firstPage
    width: 320; height: 480
    color: "white"

    Text {
        id: helloTxt
        text: "Hello World"
        y: 30
        anchors.horizontalCenter: firstPage.horizontalCenter
        font.pointSize: 25; font.bold: true
    }
}
